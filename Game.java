public class Game {
    private int id;
    private String state;
    private Set set;

    public Game(int id, String state, Set set){
        this.id = id;
        this.state = state; // can be either standard, deuce or tie-break
        this.set = set;
    }


    public int getId(){
        return this.id;
    }

    public String getState(){
        return this.state;
    }

    public Set getSet(){
        return this.set;
    }
    public boolean isStandard(){
        return (this.state == "standard") ? true : false;
    }

    public boolean isDeuce(){
        return  (this.state == "deuce") ? true : false;
    }

    public boolean isTieBreak(){
        return  (this.state == "tie-break") ? true : false;
    }

    public void setStandard(){
        this.state = "standard";
    }
    public void setDeuce(){
        this.state = "deuce";
    }
    public void setTieBreak(){
        this.state = "tie-break";
    }

}
