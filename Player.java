import java.util.HashMap;

public class Player {
    private int id;
    private String name;
    private int currentScore;
    private boolean advantage;
    private int gamesWon;
    private int setsWon;
    private HashMap<Integer,Integer> scoreBoard ;


    public Player(int id, String name){
        this.id = id;
        this.name = name;
        this.currentScore = 0;
        this.gamesWon = 0;
        this.setsWon = 0;
        this.advantage = false;
        this.scoreBoard = new HashMap<Integer, Integer>();
    }

    public HashMap<Integer,Integer> getScoreBoard(){
        return this.scoreBoard;
    }
    public int getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public int getCurrentScore(){
        return this.currentScore;
    }

    public int getGamesWon(){
        return this.gamesWon;
    }
    public int getSetsWon(){
        return this.setsWon;
    }

    public void resetScore(){
        this.currentScore = 0;
    }
    public boolean hasAdvantage(){
        return this.advantage;
    }

    public void gainAdvantage(){
        this.advantage = true;
    }

    public void loseAdvantage(){
        this.advantage = false;
    }
    public int getGamesWonInSet(int SetId){
        return this.scoreBoard.get(SetId);
    }

    public boolean scorePoint(Game game, Set currentSet){
        if (game.isStandard()) {
            switch (this.currentScore){
                case 0:
                    this.currentScore = 15;
                    break;
                case 15:
                    this.currentScore = 30;
                    break;
                case 30:
                    this.currentScore = 40;
                    break;
                case 40:
                    this.currentScore = 0;
                    this.winGame(currentSet.getId());
                    return true;
            }

        } else if (game.isDeuce()) {
            if (this.hasAdvantage()){
                this.winGame(currentSet.getId());
                return true;
            } else {
                this.gainAdvantage();
                return false;
            }

        } else {
            // this is a tie-break game
            this.currentScore += 1;
            return false;
        }
        return false;
    }

    public void winGame(int setId){
        this.gamesWon = this.gamesWon + 1 ;
        this.scoreBoard.put(setId, this.scoreBoard.get(setId)+1);

    }

    public void winSet(){
        this.setsWon = this.setsWon + 1 ;
    }

    public void initNewSet(int setId){
        this.scoreBoard.put(setId,0);
    }



}
