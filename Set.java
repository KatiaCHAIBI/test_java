import java.util.ArrayList;

public class Set {
    private int id;
    private ArrayList<Game> games;

    public Set(int id){
        this.id = id;
        this.games = new ArrayList<Game>();
    }

    public int getId(){
        return this.id;
    }

    public ArrayList<Game> getGames(){
        return this.games;
    }

    public Game getOnGoingGame(){
        return this.games.get(this.games.size()-1);

    }

    public void startNewGame(String state){
        this.games.add(new Game(this.games.size() + 1, state, this ));
    }

}
