import java.util.ArrayList;
import java.lang.Math;

public class Match {
    private int id;
    private String status;
    private Player firstPlayer;
    private Player secondPlayer;
    private ArrayList<Set> sets;
    private Player winner;



    public Match(Player firstPlayer, Player secondPlayer){
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.sets = new ArrayList<Set>();
        this.status = "Not yet started";
    }

    private String getStatus(){
        return this.status;
    }

    public void startNewSet(){
        int setId = this.sets.size()+1;
        this.sets.add(new Set(setId));
        this.firstPlayer.initNewSet(setId);
        this.secondPlayer.initNewSet(setId);
    }

    public Player getTheWinner(){
        return this.winner;
    }

    public Set getOnGoingSet(){
        return this.sets.get(this.sets.size()-1);

    }
    public void resetPlayersScores(){
        this.firstPlayer.resetScore();
        this.secondPlayer.resetScore();
        this.firstPlayer.loseAdvantage();
        this.secondPlayer.loseAdvantage();
    }

    public void displayCurrentScore(){
        System.out.println("Status :"+this.status);
        System.out.println("Score :");
        for (int key: this.firstPlayer.getScoreBoard().keySet()){
            System.out.print("("+this.firstPlayer.getScoreBoard().get(key)+"-"+this.secondPlayer.getScoreBoard().get(key)+")");
        }
        System.out.println("\n");
    }


    public void startMatch(){
        this.status = "In progress";
        this.startNewSet();
        this.getOnGoingSet().startNewGame("standard");


        while (this.firstPlayer.getSetsWon() != 3 && this.secondPlayer.getSetsWon() != 3){
            if(this.getOnGoingSet().getOnGoingGame().isStandard()){
                if (this.make_a_play(this.getOnGoingSet().getOnGoingGame(), this.getOnGoingSet())) {
                    // a player has won game, now start another one or another set depending on win conditions

                    if(this.firstPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= 6 &&
                            this.secondPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= 6){
                        this.getOnGoingSet().startNewGame("tie-break");
                        this.resetPlayersScores();

                    } else if(this.firstPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= 6 &&
                            (this.firstPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= this.secondPlayer.getGamesWonInSet(this.getOnGoingSet().getId())+2)) {

                        this.firstPlayer.winSet();
                        this.displayCurrentScore();
                        this.resetPlayersScores();
                        this.startNewSet();
                        this.getOnGoingSet().startNewGame("standard");
                    } else if (this.secondPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= 6
                            && (this.secondPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= this.firstPlayer.getGamesWonInSet(this.getOnGoingSet().getId())+2)) {
                        this.secondPlayer.winSet();
                        this.displayCurrentScore();
                        this.resetPlayersScores();
                        this.startNewSet();
                        this.getOnGoingSet().startNewGame("standard");
                    } else {
                        this.getOnGoingSet().startNewGame("standard");
                        this.resetPlayersScores();
                    }


                } else if(this.firstPlayer.getCurrentScore() == 40 && this.secondPlayer.getCurrentScore() == 40) {
                    // game still going, verify its statuss
                    // set the game to be Deuce
                    this.getOnGoingSet().getOnGoingGame().setDeuce();
                }

            } else if(this.getOnGoingSet().getOnGoingGame().isDeuce()){
                this.displayCurrentScore();
                while(!this.make_a_play(this.getOnGoingSet().getOnGoingGame(), this.getOnGoingSet())){
                    ;
                }
                if(this.firstPlayer.getGamesWonInSet(this.getOnGoingSet().getId())== 6 &&
                        this.secondPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) == 6){
                    this.getOnGoingSet().startNewGame("tie-break");

                } else if(this.firstPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= 6 &&
                        (this.firstPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= this.secondPlayer.getGamesWonInSet(this.getOnGoingSet().getId())+2)) {
                    this.firstPlayer.winSet();
                    this.displayCurrentScore();
                    this.resetPlayersScores();
                    this.startNewSet();
                    this.getOnGoingSet().startNewGame("standard");
                } else if (this.secondPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= 6 &&
                        (this.secondPlayer.getGamesWonInSet(this.getOnGoingSet().getId()) >= this.firstPlayer.getGamesWonInSet(this.getOnGoingSet().getId())+2)) {
                    this.secondPlayer.winSet();
                    this.displayCurrentScore();
                    this.resetPlayersScores();
                    this.startNewSet();
                    this.getOnGoingSet().startNewGame("standard");
                }else {
                    this.resetPlayersScores();
                    this.getOnGoingSet().startNewGame("standard");
                }

            } else{
                // it's a tie-break
                this.make_a_play(this.getOnGoingSet().getOnGoingGame(), this.getOnGoingSet());
                if(this.firstPlayer.getCurrentScore() >= 7 && (this.firstPlayer.getCurrentScore() >= this.secondPlayer.getCurrentScore()+2)) {
                    this.firstPlayer.winGame(this.getOnGoingSet().getId());
                    this.firstPlayer.winSet();
                    this.displayCurrentScore();
                    this.resetPlayersScores();
                    this.startNewSet();
                    this.getOnGoingSet().startNewGame("standard");
                } else if (this.secondPlayer.getCurrentScore() >= 7 && (this.secondPlayer.getCurrentScore() >= this.firstPlayer.getCurrentScore()+2)) {
                    this.secondPlayer.winGame(this.getOnGoingSet().getId());
                    this.secondPlayer.winSet();
                    this.displayCurrentScore();
                    this.resetPlayersScores();
                    this.startNewSet();
                    this.getOnGoingSet().startNewGame("standard");
                }
            }

        }
        if (this.firstPlayer.getSetsWon() ==  3) this.winner = this.firstPlayer;
        else this.winner = this.secondPlayer;
        status = "Finished";

    }

    public boolean make_a_play(Game currentGame, Set currentSet){
        if ((int)Math.round(Math.random()) == 0){

            return this.firstPlayer.scorePoint(currentGame, currentSet);
        } else {

            return this.secondPlayer.scorePoint(currentGame, currentSet);
        }
    }

    public static void main(String[] args){
        Player player_1 = new Player(1, "Roger Federer");
        Player player_2 = new Player(2, "Rafael Nadal");
        Match finial_US_open_tournament = new Match(player_1, player_2);
        System.out.println("Player 1 : "+ player_1.getName());
        System.out.println("Player 2 : "+ player_2.getName());
        finial_US_open_tournament.startMatch();
        System.out.println("Winner : "+ finial_US_open_tournament.getTheWinner().getName());
        System.out.println("Status : "+finial_US_open_tournament.getStatus());

    }

}
